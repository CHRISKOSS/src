//Curtain rod holder to hang all my filament in my office


rod_diameter = 21;
rod_holder_wall_thickness = 3;
dist_from_wall_front = 150;
dist_from_wall_back = 50;
hangar_height = 90;
hangar_wall_thickness = 15;
width = 25;
difference(){
union() {
//frame

cube([hangar_height, hangar_wall_thickness, width]); 
translate([0, hangar_wall_thickness, 0])holder_arm();
translate([hangar_height, hangar_wall_thickness, 0])holder_arm();
}
//negatives


 translate([hangar_height*0.75, -30, width/2]) rotate([-90,0,0]) screw_hole();
 translate([hangar_height*0.25, -30, width/2]) rotate([0,0, 0]) rotate([-90,0,0]) screw_hole();
//rotate([-90,0,0]) screw_hole();
}






screw_shaft_dia = 5.5;
screw_head_dia = 11;
screw_shaft_len = 40;

//!screw_hole();

module screw_hole() {
    cylinder(r=screw_shaft_dia/2, h= screw_shaft_len);
    translate([0,0,screw_shaft_len]) cylinder(r1=screw_shaft_dia/2, r2=screw_head_dia/2, h= 5);
    translate([0,0,screw_shaft_len+5]) 
    cylinder(r=screw_head_dia/2, h= screw_shaft_len);
}

module holder_arm() {
    cylinder(h=45, r1=5, r2=2);
}