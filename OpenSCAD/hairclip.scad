$fn = 30;

clip_front_dist = 15;
clip_back_dist = 15;

brace_spacing = 30;
//pinBrace settings
pbheight = 15;
pbwidth = 15;
pbdepth = 3;
pbdia = 8;

usewedge = true;
wedge_extra = 8;




//base
translate([0,-clip_back_dist, 0]) 
cube([brace_spacing, pbwidth + clip_front_dist + clip_back_dist, pbdepth]);

//braces
translate([brace_spacing,0,0]) rotate([0,-90,0]) pinBrace();
translate([pbdepth,0,0]) rotate([0,-90,0]) pinBrace();
translate([0,pbwidth/2-pbdepth/2,0]) cube([brace_spacing, pbdepth, 5]);

//wedge
if(usewedge) {
difference() {
translate([0, pbwidth/2, pbdepth])
rotate([90,0,90]) linear_extrude(brace_spacing) polygon(points=[[0,0], [0,pbheight-pbdia/2-pbdepth], [pbwidth/2+clip_front_dist,0]]);
    cube([brace_spacing, pbwidth/2+wedge_extra, pbheight]);
}
}

//plate2
translate([32,0,0]) {
//base
translate([0,-clip_back_dist, 0]) 
cube([brace_spacing, pbwidth + clip_front_dist + clip_back_dist, pbdepth]);

//braces
translate([brace_spacing-pbdepth-1,0,0]) rotate([0,-90,0]) pinBrace();
translate([pbdepth+pbdepth+1,0,0]) rotate([0,-90,0]) pinBrace();
translate([0,pbwidth/2-pbdepth/2,0]) cube([brace_spacing, pbdepth, 5]);
    
//wedge
if(usewedge) {
difference() {
translate([0, pbwidth/2, pbdepth])
rotate([90,0,90]) linear_extrude(brace_spacing) polygon(points=[[0,0], [0,pbheight-pbdia/2-pbdepth], [pbwidth/2+clip_front_dist,0]]);
    cube([brace_spacing, pbwidth/2+wedge_extra, pbheight]);
}
}
}

module pinBrace() {

    difference() {
        hull() {
            linear_extrude(pbdepth) square([pbdepth,15]);
            
            translate([pbheight-pbdia/2, pbwidth/2,0]) 
            cylinder(d=pbdia, h=pbdepth);
        }
            translate([pbheight-pbdia/2, pbwidth/2,0]) pin(1.55);
    }
}



module pin(pin_diameter) {
    cylinder(r=pin_diameter, h=30);
}

//translate([-5,-15,1.45]) rotate([0,90,90])  color("green") pin(1.4);