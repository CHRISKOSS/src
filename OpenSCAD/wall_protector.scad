
/*[ wall protector dimensions ]*/

// 1 = mm, 25.4=inches
unit = 25.5; // [1,25.4]

//Number of segments in circles
$fn=36;

//Height in units
height_in_units = 1.75;

//Tube outer diameter:  the size of the hole in your wall
tube_outer_diameter = 3;

//Tube thickness: tube_od-thickness is radius of opening
tube_thickness_units = 0.125;

//Thickness of the cap
cap_thickness_in_units = 0.1;

//Distance cap extends past tube outer diameter
cap_overhang_units = 0.25;

//Shift sphere to make a nice curved rim
sphere_offset = 0; //[-10:10]

height = height_in_units*unit;
tube_outer_radius = unit * tube_outer_diameter/2; 
tube_id = tube_outer_radius - tube_thickness_units * unit;
cap_overhang = unit * cap_overhang_units; 
cap_thickness = cap_thickness_in_units * unit;


difference() {
    union() {
        //tube
        translate([0,0,-height/2])
        linear_extrude(height) 
        difference() { 
            circle(tube_outer_radius);
            circle(tube_id);
        };
           
        //cap
        translate([0,0,-height/2]) 
        linear_extrude(cap_thickness) 
        difference() { 
            circle(tube_outer_radius+cap_overhang);
            circle(tube_id);
        };
    }
    
    // cut in half
    translate([0,-tube_outer_radius*1.5,-height/2]) 
    cube([tube_outer_radius*2, tube_outer_radius*3, height*2]);
    
    //round edge
    translate([0,0,
        sphere_offset-height/2-tube_outer_radius-2*cap_thickness]) 
    sphere(tube_outer_radius*1.5);
    //negative pegs
    pegs();
}

//positive pegs
rotate([0,0,180]) pegs();



module pegs() {
translate([0, tube_outer_radius, -height/2+cap_thickness])
rotate([-90,45,0]) 
cube([cap_thickness/sqrt(2), cap_thickness/sqrt(2), cap_overhang]);
    
    translate([0, tube_id, -height/2+cap_thickness]) rotate([0,0,45]) 
cube([(tube_outer_radius-tube_id)/sqrt(2), (tube_outer_radius-tube_id)/sqrt(2), height-cap_thickness]);
}