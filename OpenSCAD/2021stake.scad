linear_extrude(5) text("2021", spacing=0.85);
difference() {
union() {
translate([7,0,0]) rotate([0,0,180]) cube([1,10,1]);
translate([20,0,0]) rotate([0,0,180]) cube([1,10,1]);
}
rotate([5,0,0]) translate([0, -20, 1]) cube([30,20,2]);
}
translate([2,0,0]) cube([20,1,1]);