use <MCAD/boxes.scad>;







//mr detail necklace
 difference() {
union() {
    translate([0,0,10]) scale(0.85)import("mrdetailbowtie2.stl", convexity=3);
    scale([1,1,13]) scale(0.85) difference() {
    import("mrdetailbowtie2.stl", convexity=3);
    translate([0,0,1]) cube([230,150,50]);
    }


}

// initials
scale(0.85) translate([111,76, 3]) rotate([90,0,180]) linear_extrude(5) scale(0.95)text("TS");


//lighter
#translate([29,56,8]) rotate([0,0,-10]) rotate([0,90,0]) rotate([-90,0,0]) roundedBox([13, 26, 100], 6, true);

#translate([159,5,8]) rotate([0,0,10]) rotate([-90, 0, 0]) cigarette();
#translate([143.5,7,8]) rotate([0,0,10]) rotate([-90, 0, 0]) cigarette();



//bottom trim
translate([0,0,-200]) cube([200,200,200]);
}





//rings
scale(0.6) {

translate([25, 169, 16]) rotate([0,90,0])  scale([1,1,0.7])rotate_extrude(angle = 360, convexity = 2) translate([3,0,0]) hull() text("0");
translate([259, 169, 16]) rotate([0,90,0]) scale([1,1,0.7])rotate_extrude(angle = 360, convexity = 2) translate([3,0,0]) hull() text("0");
}

module cigarette() {
    cylinder(r=6.5, h=104);
}


//bowtie code
//union() {
//translate([0,0,-106]) scale([0.2, 0.2, 0.4]) import("Bowtie.stl", convexity=3);
//
//intersection() {
//    translate([0,0,-105]) scale([0.2, 0.2, 0.4]) import("Bowtie.stl", convexity=3);
//translate([15, 10, 0]) rotate([0,0, 24]) linear_extrude(25) text("MR DETAIL");
//}
//
//
//difference() {
//    hull() intersection() {
//    translate([0,0,-106]) scale([0.2, 0.2, 0.4]) import("Bowtie.stl", convexity=3);
//     translate([92, 45, -2]) cube([20, 32, 18]);
//    }
//    translate([111,76, 3]) rotate([90,0,180]) linear_extrude(5) text("TS");
//}