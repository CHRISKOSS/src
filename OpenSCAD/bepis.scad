// bepis coozy

$fn=30;

can_diameter = 67;//66.3;
can_height = 105;
lip_height = 20;
wall_thickness = 10;
handle_od = 80;
handle_thickness = 20;
difference() {
translate([can_diameter/2+wall_thickness,handle_thickness/2, can_height/2-wall_thickness]) rotate([90,0,0])
difference(){
    cylinder(r=handle_od/2, h=handle_thickness);
    cylinder(r=handle_od/2-handle_thickness, h=handle_thickness);
    
}
can();
}


mug_body(); 

module mug_body() {
difference() {
cylinder(r=can_diameter/2+wall_thickness, h=can_height-lip_height);
translate([0,0,wall_thickness]) can();
}
}

module can() {
    cylinder(r=can_diameter/2, h=can_height);
}