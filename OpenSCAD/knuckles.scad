union() {
    import("knuckles2.stl", convexity=5);
    
intersection() {
    translate([0,-0.5,0]) import("knuckles2.stl", convexity=5);
    translate([39, 5, 2.7]) rotate([90,0,0]) linear_extrude(10) text("SOC 'em");
}



translate([16.5,59,7.25])
rotate([0,0,20]) rotate([0,90,0]) rotate([90,0,0]) 
 scale(0.12) linear_extrude(10) import(file = "ds.svg", center = true, dpi = 96);
 
translate([47.7,67.25,7.25])
rotate([0,0,14]) rotate([0,90,0]) rotate([90,0,0]) 
 scale(0.12) linear_extrude(11) import(file = "ds.svg", center = true, dpi = 96);
 
translate([84.9,67.25,7.25])
rotate([0,0,-14]) rotate([0,90,0]) rotate([90,0,0]) 
 scale(0.12) linear_extrude(11) import(file = "ds.svg", center = true, dpi = 96);
 
 translate([116.5,59,7.25])
rotate([0,0,-20]) rotate([0,90,0]) rotate([90,0,0]) 
 scale(0.12) linear_extrude(10) import(file = "ds.svg", center = true, dpi = 96);
}