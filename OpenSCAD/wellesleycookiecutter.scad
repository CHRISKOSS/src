
cookieCutter("s");
font = "Cooper Black:style=Regular";
//font = "Californian FB:style=Italic";
offset = 0;

module shapePath(word) {
//    scale(15) rotate([0,180,0]) offset(offset) text(word, spacing=0.8, font=font);
    scale (1.5) offset(1.2) import(file = "wellesley.svg", center = true, dpi = 96);
}

module cookieCutter(
   word, 
   height = 20,
   lip = 7.5,
   wallwidth = 1.2
) {
    difference() {
    union()
        {
            linear_extrude(wallwidth) offset(lip) hull() shapePath(word);
            linear_extrude(height) offset(wallwidth) shapePath(word);
        }
    
    linear_extrude(height) shapePath(word);
    }
}

