use <MCAD/boxes.scad>;
difference() {
roundedBox([150, 70, 30], 5, false);
translate([0,0,20]) cube([200,200,10],true);
translate([0,0,10]) roundedBox([140, 60, 40], 5, false);


translate([-68,-34.5,-8]) rotate([90,0,0]) linear_extrude(1) scale([0.8, 1.5, 1]) text("WE LOVE YOU GRANDAD!");
translate([55,34.5,-8]) rotate([90,0,180]) linear_extrude(1) scale([1, 1.5, 1])text("WALLY WILLIAMS");
}