
height = 40*2;
twist = 1440*2;
coils = 2;
thickness = 1;
spring_radius =40;
coil_radius = 30;
coil_slices = height*2;
alternate_dir = false; 

top_thickness = 2;
bottom_thickness = 2;
cap_overhang = -40;
intersection() {
union() {

//Coils
for(coil = [0:1:coils]) {
    //Repeat coils with slight offset for more thickness
    for(thick = [0:1:thickness]) {
        rotate([0,
                0,
                coil*360/coils + coil_radius*thick]) 
        linear_extrude(height = height, 
                       twist = alternate_dir && coil %2 == 0 ? 
                                    twist: 
                                    -twist,
                       scale = 1,
                       center = true,
                       slices = coil_slices)
        translate([spring_radius,0,0]) 
        circle(coil_radius, $fn=6);
    }

}}
sphere(50);
}


$fn=36;
    //Top Circle            
    translate([0,0,height/2-1]) 
    linear_extrude(top_thickness) 
    difference() { 
        circle(spring_radius+coil_radius+cap_overhang);
        circle(spring_radius-coil_radius);
    }

    //Bottom circle
    translate([0,0,-height/2]) 
    linear_extrude(bottom_thickness) 
    difference() { 
        circle(spring_radius+coil_radius+cap_overhang); 
        circle(spring_radius-coil_radius);
    }
