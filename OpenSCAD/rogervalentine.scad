union() {
translate([0, 50, 0]) linear_extrude(10) text("OH  MY  GOD", spacing=0.94, size=9);

translate([0, 40, 0]) linear_extrude(10) text("I LOVE YOU", spacing=0.95);

translate([0, 33, 0]) linear_extrude(10) text("MORE THAN ROGER", spacing=0.89, size=6);


translate([0, 16, 0]) linear_extrude(10) text("LOVES", spacing=1.02, size=16);

translate([0, 7, 0]) linear_extrude(10) text("CHOCODILES", spacing=1.02, size=8);

translate([0, -2, 0]) linear_extrude(10) text("OH  MY  GOD", spacing=0.94, size=9);

translate([5,1,0]) linear_extrude(2) offset(5) square([87,54]);
//translate([5,1,0]) offset(5) square([64,54]);
translate([85,0,1]) scale([0.45, 0.45, 0.4]) rotate([-90,35,0]) import("roger.stl");
}