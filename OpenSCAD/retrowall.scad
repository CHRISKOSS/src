module pointybase() {
    for(j= [0:1:10]){
        for(i= [0:1:10]){
            if(i!=10 || i==10 && j % 2 == 0) {
            translate([j%2 ==0 ? i*10 : i*10 +5, 
            j*10, 0])
            rotate([45, 35.264, 0])
            cube(10);
            }
        }
    }
}

pointybase();
translate([-2,-10,-8]) cube([5, 120, 20]);
