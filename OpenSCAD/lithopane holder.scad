use <MCAD/boxes.scad>;

back_sidelen = 55.5;
tube_offset = 22;
wall_thickness = 5;
base_height = 4.5;

//tube thang
difference() {
translate([0,-tube_offset,(back_sidelen+base_height)/2+wall_thickness+1]) rotate([90,0,0]) 
    roundedBox([back_sidelen+wall_thickness*2, 
                back_sidelen+wall_thickness*2+base_height+2, 
                12], 2, true);
translate([0,-tube_offset,back_sidelen/2+wall_thickness+base_height+2]) rotate([90,0,0]) roundedBox([back_sidelen, back_sidelen, 12], 4, true);
} 
difference() {
    hull() {
cylinder(h=base_height, r=39.5); 
//translate([0,5,0]) cylinder(h=base_height, r=39);  
    }  

translate([0, 0,1])bottom();
translate([-50, -125,0]) cube([100,100,100]);
}

//bottom();

gap_slop = 1;//0.5;
module bottom() {

    difference() {
    cylinder(h=base_height, r=37+gap_slop); 
    translate([0,0,-0.5]) cylinder(h=base_height+1, r=35-gap_slop);
    rotate([0,0,155]) translate([-50,0,-0.5]) cube([100,100,10]);
    rotate([0,0,-155]) translate([-50,0,-0.5]) cube([100,100,10]);
    }
//    #intersection() {
//    import(file = "ABlitho.stl", center = true, dpi = 96);
//    cylinder(h=6, r=38); 
//    }
}