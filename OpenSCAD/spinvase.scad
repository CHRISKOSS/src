$fn=6;
basePrimitive();
r2d= 180/PI;

rads_per_step = 3;

firststep = 2;
stepsize = 1;
maxsteps = 500;

minsize = 4;

module basePrimitive(size) {
//   sphere(size);
    rotate([60,-25,45])
    cube(size=[size,5,25]);
}

function dist_from_center(i) = i/log(i);
function height(i) = i/log(i);
function size(i) = max(i/log(i)/8, minsize);

for(i = [firststep:stepsize:maxsteps]) {
    rotate([0,0, rads_per_step*r2d*i%360])
    translate([dist_from_center(i), 0, height(i)])
    basePrimitive(size(i));
}




//
////bowl
//
//for(i = [firststep:stepsize:maxsteps]) {
//    rotate([0,0, rads_per_step*r2d*i%360])
//    translate([dist_from_center(i), 0, height(i)])
//    basePrimitive(size(i));
//}
//
//for(i = [firststep:stepsize:maxsteps/8]) {
//    rotate([180,0,0])
//    translate([0,0,-30])
//    rotate([0,0, rads_per_step*r2d*i%360])
//    translate([dist_from_center(i), 0, height(i)])
//    basePrimitive(size(i));
//}



//
//for(i = [-99:stepsize:0]) {
//    rotate([0,0, rads_per_step*r2d*i%360])
//    translate([dist_from_center(i), 0, height(i)])
//    basePrimitive(size(i+100));
//}