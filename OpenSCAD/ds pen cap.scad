// DS pen cap
cap_height = 33;
//cap
rotate([90,0,0]) {
    difference() {
        cylinder(r=6.3, h=cap_height, $fn=6);
        translate([0,0,28]) cylinder(r=5, h=cap_height+1, $fn=6);
        #translate([0, 0, 29]) rotate([180,0,0]) cylinder(r1=5, r2=3, h=28, $fn=6);
        //cube([100,100,100]);
    }
    
    
}
//translate([0,-cap_height, 0]) rotate([90,0,0]) 
//    difference() {
//        cylinder(r=6.3, h=0.3, $fn=6);
//        cylinder(r=4.9, h=0.3, $fn=6);
//    }
    


translate([0,0.5,0]) rotate([0,30,0]) rotate([90,0,0]) scale(.115) linear_extrude(5) import(file = "ds.svg", center = true, dpi = 96);
