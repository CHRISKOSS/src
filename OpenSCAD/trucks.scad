
module v1() {
difference() {
union() {
import("trucksbase.stl", convexity=3);

scale([1,1,0.9]) hull() {
translate([40, 0, 29]) sphere(30);
translate([-40, 0, 29]) sphere(30);
}
}

translate([0,0,-100]) cube(200, center=true);
}
}
//#v1();
v2();

import("trucksv2.stl", convexity=3);

module v2() {
baseplate_thickness = 8;
baseplate_corner_rad = 3;
hole_dist_length = 53.975;
hole_dist_width = 41.275;
hole_overhang = 5;
hole_diameter = 3;

$fn = 24;
baseplate();
    
    
    
    
    
    
module baseplate() {
difference() {
hull() {
translate([hole_dist_width/2+hole_overhang, 
            hole_dist_length/2+hole_overhang, 0]) 
    cylinder(r=baseplate_corner_rad, h=baseplate_thickness);
translate([hole_dist_width/2+hole_overhang, 
            -hole_dist_length/2-hole_overhang, 0]) 
    cylinder(r=baseplate_corner_rad, h=baseplate_thickness);
translate([-hole_dist_width/2-hole_overhang, 
            hole_dist_length/2+hole_overhang, 0]) 
    cylinder(r=baseplate_corner_rad, h=baseplate_thickness);
translate([-hole_dist_width/2-hole_overhang, 
            -hole_dist_length/2-hole_overhang, 0]) 
    cylinder(r=baseplate_corner_rad, h=baseplate_thickness);
}


translate([hole_dist_width/2, hole_dist_length/2, -1]) cylinder(r=hole_diameter, h=20);
translate([hole_dist_width/2, -hole_dist_length/2, -1]) cylinder(r=hole_diameter, h=20);
translate([-hole_dist_width/2, hole_dist_length/2, -1]) cylinder(r=hole_diameter, h=20);
translate([-hole_dist_width/2, -hole_dist_length/2, -1]) cylinder(r=hole_diameter, h=20);
}
}
}

blobassembly();

module blobassembly() {
    hull() {
    theblob();

    translate([40,0,0]) cylinder(r=10);
    translate([-40, 0,0]) cylinder(r=10);
    }
    hull() {
    scale([1,1,0.7]) translate([0, 10, 60]) sphere(30);
    translate([0,30,0]) cylinder(r=5);
    translate([0,-35,0]) cylinder(r=5);
    }
}

module theblob() {
    scale([1,1,0.7]) hull() {
translate([40, 10, 60]) sphere(30);
translate([-40, 10, 60]) sphere(30);
}

}

 translate([-2, -20, 40]) rotate([-25,0,0]) rotate([0,90,0]) rotate([90,0,0]) scale(0.5) linear_extrude(5) text("Blobby v2.1");