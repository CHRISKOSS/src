canradius = 33.5;
canheight =122;
wallwidth = 2;
cutdepth = 2;
steeringwheelradius = 9 * 25.4;
hookinset = 5;

module can() {

    cylinder(h=canheight, r = canradius);
}


// canholder
difference() {
cylinder(h= canheight + wallwidth,
         r= canradius + wallwidth);
can();
//cutslice(10,5);
//cutslice( canheight + wallwidth- 10 - 5, 5);
    
#cylinder(r=20, h=130);
    
scale([1,1,1.05]) 
rotate([0,0,20]) {   
for (j =  [0 : 5] ) {   
for ( i = [0 : 6] ){
cutdiamond(10 + 15*j, 30 * i);
cutdiamond(17.5 + 15*j, 15 + 30 * i);
} 
}
}
}

// hooks
translate([-20,0,hookinset])
hook();

translate([0,0, canheight + wallwidth - hookinset])
rotate([0,180,0]) 
translate([-20,0,0])
hook();




module cutslice(height, thickness) {
    
intersection() {
difference() {
cylinder(h= canheight + wallwidth,
         r= canradius + wallwidth);
cylinder(h= canheight + wallwidth,
         r= canradius + wallwidth - cutdepth);
}

translate([0,0,height])
cylinder(h=thickness,
         r= canradius + wallwidth + 5);
}
}
//
//module steeringwheel() {
//    cylinder(h= 50, r= steeringwheelradius);
//}
//
//steeringwheel(); 
//cutslice(20, 20);


module hook() {

    hookh = 5;
    hookl = 50;
    hookthick = 5;
    hookrad = 22;
    hookcutdist = 10;
    translate([0,canradius-4,0]) {
    cube([hookthick, hookl, hookh]);
    
////    scale([2,1,1])
    //round bit
    difference() { 
    translate([hookrad,hookl,0]) 
    cylinder(h = hookh, r = hookrad); 
    translate([hookrad,hookl,0]) 
    cylinder(h = hookh, 
            r = hookrad-hookthick);
    cube([hookrad*5, hookl-hookcutdist, hookh+1]);
       translate([hookthick, 0,0])
        cube([hookthick, hookl, hookh]);
       translate([hookrad*1.78, 0,0])
        rotate([0,0,10])
        cube([hookthick, hookl, hookh]);    
    }
}
}

module cutdiamond(h, rot) { 
    cutsize = 10;
    translate([0,0,h])
    rotate([0,0,rot])
    translate([-250,0,0]) 
    rotate([45,0,0]) 
    cube([500, 10,10]);
    
}

