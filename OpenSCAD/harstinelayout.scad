use <MCAD/boxes.scad>;

show_furniture=true;

translate([100,100,0]) {

//floor
translate([0,0,-1]) color("white") cube([48,27,2], center=true);
    
//carpet
    color("#553305") {
        //living and gb1
        //master
    difference() {
    translate([-24, -13,0]) cube([47.5, 13, 0.05]);
    translate([-9.2, -13,-0.5])cube([4,10,1]);
    }
    translate([13,0,0]) cube([10.5, 13, 0.05]);
}

difference() {
    union() {
    color("#EEEEEE") resize([48,27,4]) linear_extrude(7) import(file = "harstinelayout.svg", center = true, dpi = 96);
    color("#EEEEEE") resize([48,27,8]) linear_extrude(7) import(file = "harstinelayoutwalls.svg", center = true, dpi = 96);
    }
} 
// windows
    #translate([0.2,-13.5, 2.5]) cube([6,1,4.5]);
    #translate([-18,-13.5, 2.5]) cube([3,1,4.5]);
    #translate([16.2,-13.5, 2.5]) cube([3,1,4.5]);
//}

if(show_furniture) {
//master king size bed
translate([-20,-7,0]) bed("king");

// guest br#1 bed
translate([15.65,-7.5,0]) {
bed("queen");
}


// guest br#2 bed
translate([18,9.5,0]) rotate([0,0,90]){
bed("queen");
}

//diningtable
translate([-7,4, 0]) diningtable(); 

//couch
translate([0,-12.4,0]) couch(10, 3, 3.3);

//couch2
 translate([-0.5,-2.5,0]) rotate([0,0,-90]) couch(93/12,3,3);

// coffee table
translate([ 5,-11, 0]) cube([4, 4, 1.5]);

// tv stand 
translate([10.8, -10,0]) cube([1, 7, 3]);
}
}
module bed(size) {
    translate([0,0,1.5]) {
    if(size=="king") {
        roundedBox([80/12, 76/12, 3], 5/12, true);
    } else if (size=="queen") {
        roundedBox([80/12, 60/12, 3], 5/12, true);
    } else if (size=="full") {
        roundedBox([75/12, 53/12, 3], 5/12, true);
    } else if (size=="twin") {
        roundedBox([75/12, 38/12, 3], 5/12, true);
    }
}
}

module diningtable() {
//    cylinder(r=3.5/2, h=2.5, $fn=16);
//      #translate([-2.9,-3.5,0])
//      cube([6, 8,3]);
    translate([-1,-2,0])
     cube([38/12, 70/12,3]);
        
}

module couch(l, d, h) {
//    default 9, 3, 3
    seatheight = 1.5;
    armheight = 2.25;
    armwidth = 0.75;
    cube([d, l, seatheight]);
    cube([d, armwidth, armheight]);
    translate([0, l-armwidth, 0])
    cube([d, armwidth, armheight]);
    rotate([0,-10,0]) cube([armwidth, l, h]);
    
}