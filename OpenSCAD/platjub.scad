width = 80;
offset_text_height = 1.7;
raised_text_height = 3;

name1 = "Mariam's Platinum Jubilee Tea Party";
name1y = 8;
name1offset = 0 ;

name2 = "for Her Majesty";
name2y = 12;
name2offset = 0 ;
name2bonusheight=0.5;
name2bonusx = -0;

name3 = "The Queen";
name3y = 15;
name3offset =0 ;

translate([-2,-4,0])
# cube([width+4 , name1y+name2y+name3y+4, offset_text_height]);

pole1x = 8;
pole2x = 70;
poleheight = 30;
spacing = 0.89;
font = 
//"Courier New:style=Bold";
//"Franklin Gothic Heavy:style=Regular";
//"Gadugi:style=Bold";
//"Copperplate Gothic Light:style=Bold";
"Century Schoolbook";

difference() {
union() {
    
    
translate([0,name3y,0]) {
translate([0,name2y,0]) {
 // name1
linear_extrude(raised_text_height) offset(0.15) nameSizer(width, name1y, name1);
linear_extrude(offset_text_height) offset(name1offset) nameSizer(width, name1y, name1);
}  
 // name2
translate([name2bonusx,0,0]) {
linear_extrude(raised_text_height+name2bonusheight) offset(0.15) nameSizer(width, name2y, name2);
if (name2offset > 0) {
linear_extrude(offset_text_height) offset(name2offset) nameSizer(width, name2y, name2);  
} 
}
}
    
    
// name3
linear_extrude(raised_text_height) offset(0.15) nameSizer(width, name3y, name3);
linear_extrude(offset_text_height) offset(name3offset) nameSizer(width, name3y, name3);


translate([pole1x,-poleheight+1,0])
cube([2,poleheight+1,offset_text_height]);
translate([pole2x,-poleheight+1,0])
cube([2,poleheight+1,offset_text_height]);
}
translate([-10,-50, -4.65]) cube([200, 400, 5]);
}

module nameSizer(namex, namey, name) {
    resize([namex,namey])
    text(name, spacing=spacing, font=font);
}