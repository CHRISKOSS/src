use <MCAD/boxes.scad>;
$fn = 16;
interior = [125, 25, 105];


handle = [11, 26];
difference() { 
roundedBox([129, 29, 105], 5, false);

translate([0,0,5]) roundedBox(interior, 2, true);
//roundedBox(interior, 10, false);
//    translate([0,0,-50]) cube([60,60,60]);
    
    for(i=[0:1:9]) {
        translate([-60 + i * 12,-32,-70]) cube([10, 80, 30]);
    }
    for(j=[0:1:5]) {
        for(i=[0:1:9]) {
            translate([-60 + i * 12 ,-32, -37+j*13]) cube([10, 100, 10]);
        }
    }
}



// clip

#translate([-125/2, 12.5, 44]) cube([125, 2+11+5, 5]);

#translate([-125/2, 29/2 + 11.5, 47-28]) cube([125, 5, 31]);

translate([64.5,-2,18]) rotate([0, 90,0]) linear_extrude(1) text("GMILYGM");

 translate([0,0,-51]) cube([120, 3, 3], center=true);