
armwidth = 2;
armlength = 36;

xcubecount = 10;
ycubecount = 5;
zcubecount = 3;






for(zpos = [0 : zcubecount-1]){
    for(ypos = [0 : ycubecount-1]){
        for(xpos = [0 : xcubecount-1]){
            translate([(armlength-armwidth)*xpos, (armlength-armwidth)*ypos,(armlength-armwidth)*zpos])
            singleCube(armlength, armwidth);
        }
    }
}


module singleCube(armlength, armwidth) {
    //x-arms

    cube([armlength,armwidth,armwidth]);
    translate([0, 0, armlength- armwidth])
    cube([armlength,armwidth,armwidth]);
    translate([0, armlength- armwidth, 0])
    cube([armlength,armwidth,armwidth]);
    translate([0, armlength- armwidth, armlength- armwidth])
    cube([armlength,armwidth,armwidth]);
    
    //y-arms
    cube([armwidth,armlength,armwidth]);
    translate([0, 0, armlength- armwidth])
    cube([armwidth,armlength,armwidth]);
    translate([armlength- armwidth, 0, 0])
    cube([armwidth,armlength,armwidth]);
    translate([armlength- armwidth, 0,  armlength- armwidth])
    cube([armwidth,armlength,armwidth]);
    
    //z-arms
    cube([armwidth,armwidth,armlength]);
    translate([0, armlength- armwidth, 0])
    cube([armwidth,armwidth,armlength]);
    translate([armlength- armwidth, 0, 0])
    cube([armwidth,armwidth,armlength]);
    translate([armlength- armwidth, armlength- armwidth, 0])
    cube([armwidth,armwidth,armlength]);
}
