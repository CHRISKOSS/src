

difference() {
cube([112,95,10]);
    
for(j= [1:2:11]){
        for(i= [1:1:11]){
            translate([i*10-4, j*8, 10]) sphere(4.5);
        }
        if (j!=11) {
            for(b= [1:1:10]){
                translate([b*10+1, j*8+8, 10]) sphere(4.5);
            }
        }
    }
}