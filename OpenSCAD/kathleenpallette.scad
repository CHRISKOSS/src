// kathleen paint pallette
$fn = 24;



difference() {
difference() {
    intersection() {
    translate([0,0,95]) sphere(150);

    cylinder(r=120, h=11.5);
    }

    translate([0,0,5])
    cylinder(r1=60,r2=70, h=19);



    for(a= [0:11]) {
        rotate([0,0,a*360/12])
        translate([92,0,15]) scale([1,1,0.5]) sphere(20);
    }
    
    
//    cube([150,150,100]);

    



}

    translate([0,0,6])
    circletext("@ kat.shearer @ kat.shearer @ kat.shearer @ kat.shearer @ kat.shearer @ kat.shearer @ kat.shearer @ kat.shearer @ kat.shearer @ kat.shearer",textsize=5,degrees=360,top=false, myfont="Calibri:style=Bold");
}


module
circletext(mytext,textsize=10,myfont="Arial",radius=120-4,thickness=1,degrees=360,top=true){
       
        chars=len(mytext)+1;
       
        for (i = [1:chars]) {
                        rotate([0,0,(top?1:-1)*(degrees/2-i*(degrees/chars))])
                        translate([0,(top?1:-1)*radius-(top?0:textsize/2),0])
                        //linear_extrude(5)
            rotate([90,0,0])
            linear_extrude(2)
                        text(mytext[i-1],halign="center",font=myfont,size=textsize);
        }
}

  /*  translate([0,0,5])
circletext("Mariam Sulay",textsize=5,degrees=360,top=false);*/


echo(version=version());
// Written in 2015 by Torsten Paul <Torsten.Paul@gmx.de>
//
// To the extent possible under law, the author(s) have dedicated all
// copyright and related and neighboring rights to this software to the
// public domain worldwide. This software is distributed without any
// warranty.
//
// You should have received a copy of the CC0 Public Domain
// Dedication along with this software.
// If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
