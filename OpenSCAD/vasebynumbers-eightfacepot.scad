$fn = 8;
layerheight = 5;
radii = [25, 20, 24.8, 29, 33, 36, 37.2, 38, 37.2, 36, 33, 27.5, 21, 15, 12, 15, 19, 25];

bottom_thickness = 0.5;
wall_thickness = 0.5;

for (i=[0:1:len(radii)-2]) {
    difference() {
    // outer wall
    translate([0,0, layerheight * i])
    cylinder(h=layerheight, r1=radii[i], r2=radii[i+1]);
    translate([0,0, layerheight * i])
    // inner wall
    cylinder(h=layerheight, r1=radii[i]-wall_thickness, r2=radii[i+1]-wall_thickness);
    }
}
// base
translate([0,0,-bottom_thickness]) cylinder(h=bottom_thickness, r=radii[0]);


//handles
difference() {
    union() {
        translate([33,0,48]) rotate([90, 0, 0]) handle(15, 1.5);
        rotate([0,0,180])
        translate([33,0,48]) rotate([90, 0, 0]) handle(15, 1.5);
    }
    for (i=[0:1:len(radii)-2]) {
        translate([0,0, layerheight * i])
        cylinder(h=layerheight, r1=radii[i], r2=radii[i+1]);
    }
}

module handle(radius, stretch) {
    pct_cutoff = 0.55;
    scale([1,stretch,0.5])
    difference() {
        sphere(radius);
        translate([0,0, -radius]) cylinder(h=radius*2, r=radius*4/5);
        translate([0,0,radius*pct_cutoff]) cylinder(h=radius, r=radius);
        translate([0,0,radius*-(1+pct_cutoff)]) cylinder(h=radius, r=radius);
    }
    
}