// name flower pot
$fn = 60;




difference() {
    intersection() {
    translate([0,0,35]) sphere(50);
    translate([0,0,25]) sphere(50);
    cylinder(r=37, h=60);
    }
    translate([0,0,5]) cylinder(r=35, h=60);
    



    for(a= [0:7:50]) {
        
    translate([0, 0 , 3+a])
    rotate(a*5)
    circletext("MARIAM SULAYMAN KOSS MARIAM SULAYMAN KOSS",textsize=5,degrees=360,top=false);
    }
    translate([15,0,-10]) cylinder(r=3, h=20);
    translate([-15,0,-10]) cylinder(r=3, h=20);
    translate([0, 15,-10]) cylinder(r=3, h=20);
    translate([0, -15,-10]) cylinder(r=3, h=20);
}


module make_ring_of(radius, count)
{
    for (a = [0 : count - 1]) {
        angle = a * 360 / count;
        translate(radius * [sin(angle), -cos(angle), 0])
            rotate([0, 0, angle])
                children();
    }
}


module
circletext(mytext,textsize=20,myfont="Arial",radius=34,thickness=1,degrees=360,top=true){
       
        chars=len(mytext)+1;
       
        for (i = [1:chars]) {
                        rotate([0,0,(top?1:-1)*(degrees/2-i*(degrees/chars))])
                        translate([0,(top?1:-1)*radius-(top?0:textsize/2),0])
                        //linear_extrude(5)
            rotate([90,0,0])
            linear_extrude(1)
                        text(mytext[i-1],halign="center",font=myfont,size=textsize);
        }
}

  /*  translate([0,0,5])
circletext("Mariam Sulay",textsize=5,degrees=360,top=false);*/


echo(version=version());
// Written in 2015 by Torsten Paul <Torsten.Paul@gmx.de>
//
// To the extent possible under law, the author(s) have dedicated all
// copyright and related and neighboring rights to this software to the
// public domain worldwide. This software is distributed without any
// warranty.
//
// You should have received a copy of the CC0 Public Domain
// Dedication along with this software.
// If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
