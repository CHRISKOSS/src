//Defensestorm Pen

tip_height = 90; //previously 90
cap_height = 33;
translate([0,0,4.5])
difference() {
    union() {
    //pen outer (r was 6)
    rotate([-90,0,0]) cylinder(r=5, h=tip_height, $fn=6);

    //pen tip
    rotate([-90,0,0]) translate([0,0,tip_height]) cylinder(r1=5, r2=3, h=20, $fn=6);

    //bottom text
//    translate([-3.25,0,0]) rotate([0,180,0]) rotate([0,0,90]) scale(0.9) linear_extrude(6)text("DefenseStorm", font="Courier New:bold");

    //top text
    translate([2.5,22,3]) rotate([0,0,90]) scale(0.6) linear_extrude(3) text("Brayan Torres", font="Courier New:bold");
       
    //end logo 
    rotate([0,30,0]) rotate([90,0,0]) scale(.091) linear_extrude(7) import(file = "ds.svg", center = true, dpi = 96);
       
    //top logo 
    translate([0.06,16,1]) rotate([0,0,90]) scale(.08) linear_extrude(55) import(file = "ds.svg", center = true, dpi = 96);
    }
    

    
    
    
//    //cap ridge
//    translate([0,89,0]) rotate([-90,0,0]) difference() {
//        cylinder(r=5, h=0.4, $fn=6);
//        cylinder(r=4.6, h=0.4, $fn=6);
//    }
    

rotate([-90,0,0]) translate([0,0,tip_height - 90 + 2]) 
ink_insert();
    
//cube([100,100,100]);
}
module ink_insert() {
    cylinder(r=1.75, h=107, $fn=36);
    translate([0,0,107]) cylinder(r=2.5/2, h=5, $fn=36);
    translate([-1.75, -1.75,0]) cube([3.5, 1.75,107]);
    translate([0,0,112]) cylinder(r1=2.5/2, r2=0.6, h=2.5, $fn=36);
    translate([0,-0.5,5+80]) cube([4.5, 2.5, 10], center=true);
}


