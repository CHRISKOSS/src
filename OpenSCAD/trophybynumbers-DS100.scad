$fn = 6;
layerheight = 5;
radii = [30, 25, 20, 16, 14, 18, 22, 26.5, 29, 31.2, 33, 35, 36, 36.5, 37, 37.2, 37.4];

bottom_thickness = 0;
wall_thickness = 6.5;
bottom_layer = 4;

part1 = 
//false;
true;
part2 = 
false;
//true;

top_front_text = "100th";
bottom_front_text = "CUSTOMER";
ln1_back_text = "September";
ln2_back_text = "29th 2021";
ln3_back_text = "Brett";
ln4_back_text = "Kunkle";

if(part1) {
    difference() {
        scale([1.02,1.02,1]) body();
        // top front text
        translate([-18.5,-20,55]) 
        resize([36,40,20])
        rotate([90,0,0]) linear_extrude(20) 
        text(top_front_text, font="Bernard MT Condensed");
        // bottom front text
        translate([-15.5,-20,42]) 
        resize([30,40,10])
        rotate([90,0,0]) linear_extrude(20) 
        text(bottom_front_text, font="Bernard MT Condensed");
        
        
        //ln1 back text
        #translate([18.5,20,65]) 
        resize([36,40,10])
        rotate([90,0,180]) linear_extrude(40) 
        text(ln1_back_text, 
            font="HYShortSamul\\-Medium:style=Regular");
        //ln2 back text
        #translate([17.5,20,55]) 
        resize([34,40,8])
        rotate([90,0,180]) linear_extrude(40) 
        text(ln2_back_text, 
            font="HYShortSamul\\-Medium:style=Regular");
        //ln3 back text
        #translate([14.5,20,43]) 
        resize([28,40,8])
        rotate([90,0,180]) linear_extrude(40) 
        text(ln3_back_text, 
            font="HYShortSamul\\-Medium:style=Regular");
        //ln4 back text
        #translate([13.5,20,33]) 
        resize([26,40,8])
        rotate([90,0,180]) linear_extrude(40) 
        text(ln4_back_text, 
            font="HYShortSamul\\-Medium:style=Regular");
    }
    
    
    
        body();
    
    //handles
    difference() {
        union() {
            handle(15, 1.5);
            rotate([0,0,180])
            handle(15, 1.5);
        }
        for (i=[0:1:len(radii)-2]) {
            translate([0,0, layerheight * i])
            cylinder(h=layerheight, r1=radii[i], r2=radii[i+1]);
        }
    }
}

module body() {
union() {
for (i=[0:1:len(radii)-2]) {
    difference() {
    // outer wall
    translate([0,0, layerheight * i])
    cylinder(h=layerheight, r1=radii[i], r2=radii[i+1]);
    translate([0,0, layerheight * i])
    // inner wall
    if(i > bottom_layer) {
    cylinder(h=layerheight, r1=radii[i]-wall_thickness, r2=radii[i+1]-wall_thickness);
    } else if( i>1) {
         cylinder(h=layerheight, r=6.1);
    }
    }
}
// base
translate([0,0,-bottom_thickness+bottom_layer*layerheight]) cylinder(h=bottom_thickness, r=radii[bottom_layer]);
}
//cube(100);
//}
}




module handle(radius, stretch) {

    
    pct_cutoff = 0.50;
    translate([38,0,68])
    rotate([90, 39, 0]) 
    scale([1,stretch,1])
    difference() {
        sphere(radius);
        translate([0,0, -radius]) cylinder(h=radius*2, r=radius*1/2);
        translate([0,0,radius*pct_cutoff]) cylinder(h=radius, r=radius);
        translate([0,0,radius*-(1+pct_cutoff)]) cylinder(h=radius, r=radius);
    }
    
}


if(part2) {
  translate([0,3,140]) rotate([90,0,0])  
  union() {
      translate([0,-35,-1.7 ]) scale([0.5, 0.5, 1.1]) import ("defensestorm.stl", convexity = 4);
      translate([0,-60,3.5]) 
      rotate([90,0,0]) 
        cylinder(h=70, r=6);
  }
}