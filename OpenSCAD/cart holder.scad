// vape cart holder
use <MCAD/boxes.scad>;

difference() {
    roundedBox([35, 50, 30], 5, false);
    cart();
    translate([0,15, 0]) cart();
    translate([0,-15, 0]) cart();
    translate([0,0,-15]) roundedBox([37, 52, 20], 5, true);
    //translate([-20,23,16]) rotate([180,0,0]) scale(0.5) import("negativepointywall.stl", convexity=3);
    rotate([0,60,0]) translate([-20,25,15]) rotate([180,0,0]) scale(0.5) import("negativepointywall.stl", convexity=3);
    rotate([0,-60,0]) translate([-20,25,15]) rotate([180,0,0]) scale(0.5) import("negativepointywall.stl", convexity=3);
    
    rotate([0,60,0]) translate([0,0,23]) roundedBox([37, 52, 20], 5, true);
        rotate([0,-60,0]) translate([0,0,23]) roundedBox([37, 52, 20], 5, true);
    //cube([30,30,30]);
}
module cart() {
    translate([0,0,5]) cylinder(r=5.5, h=20);
    cylinder(r=3.75, h=20);
}

translate([5, 25, 0]) rotate([90,0,180]) linear_extrude(0.5) text("S");