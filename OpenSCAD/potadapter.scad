$fn = 120;

vase_od =91; 
vase_thickness = 2.5;
pot_od = 92.5;
pot_thickness =3 ; 



module vase() {
    difference() { 
        cylinder(r = vase_od/2, 
            h = 100);
        cylinder(r = vase_od/2 - vase_thickness,
            h = 100);
    }
}


module pot() {
    difference() { 
        cylinder(r = pot_od/2, 
            h = 100);
//        cylinder(r = pot_od/2 - pot_thickness,
//            h = 100);
    }
}

difference() {
    translate([0,0,-10]) cylinder(r=50, h=20);
    #translate([0,0,1]) vase();
    translate([0,0,-101]) pot();
    translate([0,0,-50]) cylinder(r=vase_od/2, h = 100);
//    translate([18,18,-30]) cube([100,100,100]);
}