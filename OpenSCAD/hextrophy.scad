$fn = 6;
layerheight = 5;
radii = [30, 25, 20, 16, 14, 18, 22, 26.5, 29, 31.2, 33, 35, 36, 36.5, 37, 37.2, 37.4];

bottom_thickness = 6.5;
wall_thickness = 6.5;
bottom_layer = 4;



for (i=[0:1:len(radii)-2]) {
    difference() {
    // outer wall
    translate([0,0, layerheight * i])
    cylinder(h=layerheight, r1=radii[i], r2=radii[i+1]);
    translate([0,0, layerheight * i])
    // inner wall
    cylinder(h=layerheight, r1=radii[i]-wall_thickness, r2=radii[i+1]-wall_thickness);
    }
}
// base
translate([0,0,-bottom_thickness+bottom_layer*layerheight]) cylinder(h=bottom_thickness, r=radii[bottom_layer]);


//handles
difference() {
    union() {
        handle(15, 1.5);
        rotate([0,0,180])
        handle(15, 1.5);
    }
    for (i=[0:1:len(radii)-2]) {
        translate([0,0, layerheight * i])
        cylinder(h=layerheight, r1=radii[i], r2=radii[i+1]);
    }
}
module handle(radius, stretch) {

    
    pct_cutoff = 0.50;
    translate([38,0,68])
    rotate([90, 39, 0]) 
    scale([1,stretch,1])
    difference() {
        sphere(radius);
        translate([0,0, -radius]) cylinder(h=radius*2, r=radius*1/2);
        translate([0,0,radius*pct_cutoff]) cylinder(h=radius, r=radius);
        translate([0,0,radius*-(1+pct_cutoff)]) cylinder(h=radius, r=radius);
    }
    
}

