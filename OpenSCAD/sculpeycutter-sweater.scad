//firstprint 1.2
//secondprint 1

sculpeyCutter(1);
//translate([40,0,0]) sculpeyCutter(0.3);
//translate([-45,0,0]) sculpeyCutter(0.4);
font = "Cooper Black:style=Regular";
//font = "Californian FB:style=Italic";
offset = 0;

module shapePath(scal) {
//    scale(15) rotate([0,180,0]) offset(offset) text(word, spacing=0.8, font=font);
    scale(scal) import(file ="sweaterlines.svg", center = true, dpi = 96);
}

module sculpeyCutter(
   scal, 
   height = 10,
   h2 = 12,
   lip = 5,
   wallwidth = 0
  
) {
    
//    difference() {
    union()
        {
            linear_extrude(2) offset(lip) shapePath(scal);
            
            
            linear_extrude(height) offset(wallwidth) shapePath(scal);
//            linear_extrude(h2) offset(wallwidth) scale(scal) import(file ="sweater.svg", center = true, dpi = 96);
            
        }
    
//    translate([0,0,-1]) linear_extrude(height+2) shapePath(scal);
//    }
}

