// first print at 1.8

sculpeyCutter(1.8);
//translate([40,0,0]) sculpeyCutter(0.3);
//translate([-45,0,0]) sculpeyCutter(0.4);
font = "Cooper Black:style=Regular";
//font = "Californian FB:style=Italic";
offset = 0;

module shapePath(scal) {
//    scale(15) rotate([0,180,0]) offset(offset) text(word, spacing=0.8, font=font);
    scale(scal) import(file ="snake.svg", center = true, dpi = 96);
}

module sculpeyCutter(
   scal, 
   height = 10,
   lip = 5,
   wallwidth = 1
  
) {
    
    difference() {
    union()
        {
            linear_extrude(wallwidth) offset(lip) shapePath(scal);
            
            linear_extrude(height) offset(wallwidth) shapePath(scal);
            
        }
    
    translate([0,0,-1]) linear_extrude(height+2) shapePath(scal);
    }
}
translate([-5,-2,0.5]) cube([3,55,1], center=true);
translate([7.5,-2,0.5]) cube([3,55,1], center=true);
translate([0,8,0.5]) cube([40,3,1], center=true);
