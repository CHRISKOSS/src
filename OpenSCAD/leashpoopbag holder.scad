cxtr_thickness = 3;
cxtr_top_width = 11;
cxtr_len = 20;
cxtr_middle_len = 40;


difference() {
union() {
//connector top
cube([cxtr_top_width, cxtr_thickness, cxtr_len]);
//connector middle
hull() {
translate([(cxtr_top_width-cxtr_thickness)/2 , cxtr_thickness,0])
cube([cxtr_thickness, cxtr_middle_len, cxtr_len]);

translate([0,35,0]) rotate([30,0,0]) cube([cxtr_top_width, cxtr_thickness, cxtr_len]);
}
}

$fn = 48;


//subtractions)
hull() {
translate([0, cxtr_middle_len- 5, 5]) rotate([0,90,0]) cylinder(r=2, h=30);
translate([0, cxtr_middle_len- 20, cxtr_len+5]) rotate([0,90,0]) cylinder(r=4, h=30);
}

 translate([-20,0, cxtr_len-5]) rotate([10,0,0]) cube(100);
}