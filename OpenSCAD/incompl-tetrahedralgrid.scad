$fn =3;

armlen = 60;
armrad = 2;


triarm(armlen, armrad);


/*rotate([0,0,60])
translate([sqrt(3)*armrad,-armlen - 3*armrad,0])
//rotate([0,-30,0])
translate([0,0,-5])
#cube([10, armlen + 6*armrad, 10]);*/

if (true) {

translate([0, 0, 0])
rotate([0,0,60])
triarm(armlen, armrad);


translate([0, armlen, 0])
rotate([0,0,60])
triarm(armlen, armrad);

translate([0, armlen, 0])
triarm(armlen, armrad);

}



module triarm(armlen, armrad) {
    difference() {
    union() {

    translate([0, 2*armrad, 0])
    arm(armlen+4*armrad, armrad);
    

    rotate([0, 0, 60])
    translate([0,2*armrad,0])
    arm(armlen+4*armrad, armrad);
    
    translate([sqrt(3)*armlen/2,-armlen/2,0])
    rotate([0,0, -60])
        translate([0,armrad,0])
    arm(armlen+armrad, armrad);
    }

}

}





module arm(alen, arad) {
    translate([0,0,1])
    rotate([0,270,90]) {
    cylinder(h=alen, r=arad);
    }
}