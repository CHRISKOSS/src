// first print at 1.8

sculpeyCutter(0.5);
//translate([40,0,0]) sculpeyCutter(0.3);
//translate([-45,0,0]) sculpeyCutter(0.4);
font = "Cooper Black:style=Regular";
//font = "Californian FB:style=Italic";
offset = 0;

module shapePath(scal) {
//    scale(15) rotate([0,180,0]) offset(offset) text(word, spacing=0.8, font=font);
    scale(scal) import(file ="willow-branch-svgrepo-com.svg", center = true, dpi = 96);
}

module sculpeyCutter(
   scal, 
   height = 10,
   h2 = 12,
   lip = 2,
   wallwidth = 0.8
  
) {
    
    difference() {
    union()
        {
            linear_extrude(height/2) offset(lip) shapePath(scal);
            
            linear_extrude(height) offset(wallwidth) shapePath(scal);
            
        }
    
    translate([0,0,-1]) linear_extrude(height+2) shapePath(scal);
    }
}

