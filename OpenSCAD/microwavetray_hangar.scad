$fn=48;
back_width = 29;
dist_from_back = 44;
difference() {
cube([back_width+2, 62, 2]);
translate([1,1,-0.1]) cube([back_width, 60, 1]);
}

hull(){
intersection() {
hook();
rotate([70,0,0]) cube([back_width+2, 62, 2]);
    }

translate([back_width/2, 0, dist_from_back+0.75]) rotate([-93,0,0]) rotate([0,0,45]) cylinder(r1=2, r2=5, h=6, $fn=4);
}

intersection() {
hook();
    translate([-100, 5,15]) cube(200);
}

hull() {
translate([0,0,1]) cube([back_width+2, 62, 1]);

intersection() {
translate([0,0,15]) cube([back_width+2, 62, 1]);
    hook();
}
}

module hook() {
difference() {
intersection(){
translate([back_width/2-2, 0, 2]) cube([4, 62, dist_from_back]); 
translate([back_width/2, 0, 0]) scale([1,1,0.76]) sphere(62);
}
rotate([20,0,0]) translate([back_width/2, 10, 20]) scale([1,0.9,1]) sphere(22);
}
}

//#translate([11.5,0,41.5]) cube(6);