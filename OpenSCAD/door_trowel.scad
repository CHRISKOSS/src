//// door frame trowel thingy
////
//// all units are in millimeters
////
//// I made it parametric, so you can fiddle all the 
//// magic numbers to dial in the size you want.
//// You can use the customizer panel on the right or 
//// you can just change the numbers in the code
////
//// Press F5 to preview
//// F6 to render (prerequisite for export)
//// F7 to export (save stl)
//// Then load the stl into Cura like normal
////

use <MCAD/boxes.scad>;






width = 25;
thickness = 3;
base_len = 50;
base_to_molding = 12.7; // half inch
molding_len = 50;
molding_to_trowel = 19.05; //3/4 inch

trowel_thickness = 5;
roundradius = 10;

//base
cube([base_len, thickness, width]);

//base up to molding
translate([base_len-thickness,0,0]) 
    cube([thickness, base_to_molding, width]);
    
//molding
translate([base_len-thickness,base_to_molding-thickness,0]) 
    cube([molding_len, thickness, width]);
    
difference() {
    
//'trowel'
translate([base_len-thickness+molding_len, base_to_molding - molding_to_trowel,0]) 
    cube([trowel_thickness, molding_to_trowel, width]);
    
//rounding (only parts that intesect with both are rendered)
    # translate([base_len-thickness+molding_len,
                base_to_molding - molding_to_trowel + roundradius
                ,roundradius])
    rotate([-90,0,-90]) 
    linear_extrude(trowel_thickness)
    difference(){
        square(roundradius);
        circle(roundradius, $fn = 24); //uncomment
    }
}