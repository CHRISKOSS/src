//////dsornament



top = true;
bottom = false;
diameter = 60;
thickness= 1;

if(top) balltop();
if(bottom) ballbottom();

module balltop() {
    difference() {
    ball();
    #sphere(diameter/2-thickness);
    translate([0,0,-diameter]) cylinder(r=diameter, h=diameter);
    }
}

module ballbottom() {
    difference() {
    ball();
    #sphere(diameter/2-thickness);
    cylinder(r=diameter, h=diameter);
    }
}



module ball() {
    resize([diameter,diameter,diameter])
    import(file ="dsball3.stl", center = true, convexity=3);
}