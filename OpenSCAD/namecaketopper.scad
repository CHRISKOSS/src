width = 60;
offset_text_height = 1;
raised_text_height = 5;

name1 = "HAPPY";
name1y = 10;
name1offset = 1.5;

name2 = "BIRTHDAY";
name2y = 20;
name2offset = 0;
name2bonusheight=0.5;
name2bonusx = 2;

name3 = "GRAMMY";
name3y = 30;
name3offset = 1.5;


pole1x = 10;
pole2x = 50;
poleheight = 30;
spacing = 0.89;
font = 
//"Courier New:style=Bold";
"Franklin Gothic Heavy:style=Regular";
//"Gadugi:style=Bold";

difference() {
union() {
    
    
translate([0,name3y,0]) {
translate([0,name2y,0]) {
 // name1
linear_extrude(raised_text_height) nameSizer(width, name1y, name1);
linear_extrude(offset_text_height) offset(name1offset) nameSizer(width, name1y, name1);
}  
 // name2
translate([name2bonusx,0,0]) {
linear_extrude(raised_text_height+name2bonusheight) nameSizer(width, name2y, name2);
if (name2offset > 0) {
linear_extrude(offset_text_height) offset(name2offset) nameSizer(width, name2y, name2);  
} 
}
}
    
    
// name3
linear_extrude(raised_text_height) nameSizer(width, name3y, name3);
linear_extrude(offset_text_height) offset(name3offset) nameSizer(width, name3y, name3);


translate([pole1x,-poleheight+1,0])
cube([2,poleheight+1,2]);
translate([pole2x,-poleheight+1,0])
cube([2,poleheight+1,2]);
}
translate([-10,-50, -4.65]) cube([200, 400, 5]);
}

module nameSizer(namex, namey, name) {
    resize([namex,namey])
    text(name, spacing=spacing, font=font);
}