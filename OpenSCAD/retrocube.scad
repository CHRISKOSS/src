module pointybase() {
    for(j= [0:1:10]){
        for(i= [0:1:10]){
            if(i!=10 || i==10 && j % 2 == 0) {
            translate([j%2 ==0 ? i*10 : i*10 +5, 
            j*10, 0])
            rotate([45, 35.264, 0])
            cube(10);
            }
        }
    }
}

module pointywall1() {
    for(j= [0:1:10]){
        for(i= [0:1:10]){
            if(i==0 || i == 10) {
            translate([ 
            j*10, i*10, j%2 ==0 ? 0:-5])
            rotate([45, 35.264, 0])
            cube(10);
            }
        }
    }
}
module pointywall2() {
    for(j= [0:1:10]){
        for(i= [0:1:10]){
            if(i==0 || i == 10) {
            translate([i*10, 
            j*10, j%2 ==0 ? 0:-5])
            rotate([45, 35.264, 0])
            cube(10);
            }
        }
    }
}
pointybase();

for(h= [1:1:10]) {

translate([0,0,10*h])
pointywall2();
    translate([0,0,10*h])
    pointywall1();

    
}