// tactile architect rule
$fn = 100;

scale_length = 100;
ruler_radius = 25;
scale_margin = 5;

ruler_length = scale_length + scale_margin * 2;
dash_size = 0.5;


ruler_body();




module ruler_body () {
    difference() {
        cylinder(r=ruler_radius, h=ruler_length);
        tri_cut(0);
        tri_cut(120);
        tri_cut(240);
        center_groove(0);
        center_groove(120);
        center_groove(240);
        marks(10, 5);
        
rotate([0,0,120]) marks(10,5);
rotate([0,0,240]) marks(25.4,25.4/4);
translate([0,0,scale_length + scale_margin * 2]) rotate([180,0,0]) marks(10, 5);
translate([0,0,scale_length + scale_margin * 2]) rotate([180,0,120]) marks(10, 5);
translate([0,0,scale_length + scale_margin * 2]) rotate([180,0,240]) marks(10, 5);
        
    }
    
}


module tri_cut(rot) {
    rotate([0,0,rot]) 
    translate([ruler_radius*2.87,0,-1])  cylinder(r=ruler_radius*2.5, h=ruler_length+2);
}

module center_groove(rot) {
    rotate([0,0,rot]) 
    translate([ruler_radius*0.4,0,-1])  cylinder(r=ruler_radius*0.2, h=ruler_length+2);
}

//marks(10,5);

module marks(tall_spacing, short_spacing) {
    translate([-25.2,0.9,scale_margin])  rotate([90,0,13]) {
    for(i = [0:tall_spacing:scale_length]) {
        talldash(i);
        translate([7,i+2,1]) rotate([180,0,90]) linear_extrude(5) scale(0.45) rotate([0,0,90]) text(str(i));
    }
    for(i = [0:short_spacing:scale_length]) {
        shortdash(i);
    }

}
}

module talldash(dist) {
    translate([0,dist,0]) rotate([45,0,0]) cube([7, dash_size, dash_size]);
}

module shortdash(dist) { 
    translate([0,dist,0])  rotate([45,0,0]) cube([3, dash_size, dash_size]);
}