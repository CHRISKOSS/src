/* [SPRING] */
//resolution of the circles
$fn=36;
height = 80;
//number of 360 revolutions
twists = 8;
twist = twists * 360;
coils = 2;
coil_thickness = 20;
spring_radius =15;
coil_radius = 3;
coil_slices = height*2;
// may not be a spring anymore, but looks neat
alternate_dir = false; 

top_height = 2;
bottom_height = 2;
cap_overhang = 0;

union() {

//Coils
for(coil = [0:1:coils]) {
    //Repeat coils with slight offset for more thickness
    for(thick = [0:1:coil_thickness]) {
        rotate([0,
                0,
                coil*360/coils + coil_radius*thick]) 
        linear_extrude(height = height, 
                       twist = alternate_dir && coil %2 == 0 ? 
                                    twist: 
                                    -twist,
                       scale = 1,
                       center = true,
                       slices = coil_slices)
        translate([spring_radius,0,0]) 
        circle(coil_radius, $fn=6);
    }

}


    //Top Circle            
    translate([0,0,height/2-1]) 
    linear_extrude(top_height) 
    difference() { 
        circle(spring_radius+coil_radius+cap_overhang);
        circle(spring_radius-coil_radius);
    }

    //Bottom circle
    translate([0,0,-height/2]) 
    linear_extrude(bottom_height) 
    difference() { 
        circle(spring_radius+coil_radius+cap_overhang); 
        circle(spring_radius-coil_radius);
    }
}