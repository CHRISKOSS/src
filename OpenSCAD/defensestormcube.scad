intersection() {
linear_extrude(200) rotate([0,0,7]) import(file = "ds.svg", center = true, dpi = 96);
    #translate([-50,-45,40])  rotate([37,37,30]) cube(80);
}

rotate([0,0,7]) translate([0, -30, 60]) rotate([90,0,0]) cylinder(r=3, h=15);
rotate([0,0,97]) translate([0, -25, 60]) rotate([90,0,0]) cylinder(r=3, h=15);
rotate([0,0,-83]) translate([0, -25, 70]) rotate([90,0,0]) cylinder(r=3, h=15);

//$fn = 6;
//rotate([0,0,30]) cylinder(r=55, h = 2);