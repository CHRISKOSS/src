// pentacle tree topper
diameter = 180;
thickness = 40;
hole_diameter = 32;


difference() {
rotate([0,0,36]) resize([diameter,diameter,thickness]) linear_extrude(5) import(file ="pentacle.svg", center = true, dpi = 96);
#translate([0,diameter*.3,thickness/2]) rotate([90,0,0]) cylinder(r=hole_diameter/2, h=diameter);
}

