//////dsornament - cone edition



top = true;
bottom = false;
diameter = 60;
thickness= 1;
renderball = false;

if(renderball) {
difference() {
ballwithtop();
cylinder(r=10,h=76, $fn=6);
}
}

module ballwithtop() {
difference() {
    union() {
    ball();
    cylinder(r=10,h=75, $fn=6);
    }
scale(0.98) ball();
}

}
//ballwithtop();

module ball() {
//    resize([diameter,diameter,diameter])
//    scale(0.05)
    import(file ="dsballcone2.stl", center = true, convexity=5);
}

cap();
cap_height = 12;
module cap() {
    difference() {
    union() {
        cylinder(r=5-0.05,h=cap_height, $fn=6);
        difference() {
        cylinder(r1=10, r2=14, h=cap_height, $fn=6);
        translate([0,0,5]) cylinder(r1=10, r2=14, h=20, $fn=6);
        }
    } 
    scale([1.5,0.75,1]) sphere(5); 
    
    
    
    // cuts
    cut_h = 10;
    cut_r = 2;
    cut_r_mult = 20;
    cut_fn = 3;

    
    for (i = [0:5]) {
    
    rotate([0,0,60*i]) translate([10, 0, cap_height]) rotate([0,90,0]) cylinder(r1=cut_r, r2=cut_r*cut_r_mult, h=cut_h, $fn=cut_fn); 
    }
    }
    translate([-1,-5,0]) cube([2, 10, 1]);
}

//
//translate([40,0,0]) import(file ="dsornamentcone-cap.stl", center = true, convexity=5);