$fn = 6;
layerheight = 5;
radii = [30, 25, 20, 16, 14, 18, 22, 26.5, 29, 31.2, 33, 35, 36, 36.5, 37, 37.2, 37.4];

bottom_thickness = 0;
wall_thickness = 6.5;
bottom_layer = 4;

part1 = true;
part2 = true;

if(part1) {
//difference() {
union() {
for (i=[0:1:len(radii)-2]) {
    difference() {
    // outer wall
    translate([0,0, layerheight * i])
    cylinder(h=layerheight, r1=radii[i], r2=radii[i+1]);
    translate([0,0, layerheight * i])
    // inner wall
    if(i > bottom_layer) {
    cylinder(h=layerheight, r1=radii[i]-wall_thickness, r2=radii[i+1]-wall_thickness);
    } else if( i>1) {
         cylinder(h=layerheight, r=6.1);
    }
    }
}
// base
translate([0,0,-bottom_thickness+bottom_layer*layerheight]) cylinder(h=bottom_thickness, r=radii[bottom_layer]);
}
//cube(100);
//}


//handles
difference() {
    union() {
        handle(15, 1.5);
        rotate([0,0,180])
        handle(15, 1.5);
    }
    for (i=[0:1:len(radii)-2]) {
        translate([0,0, layerheight * i])
        cylinder(h=layerheight, r1=radii[i], r2=radii[i+1]);
    }
}
}
module handle(radius, stretch) {

    
    pct_cutoff = 0.50;
    translate([38,0,68])
    rotate([90, 39, 0]) 
    scale([1,stretch,1])
    difference() {
        sphere(radius);
        translate([0,0, -radius]) cylinder(h=radius*2, r=radius*1/2);
        translate([0,0,radius*pct_cutoff]) cylinder(h=radius, r=radius);
        translate([0,0,radius*-(1+pct_cutoff)]) cylinder(h=radius, r=radius);
    }
    
}


if(part2) {
  translate([0,3,140]) rotate([90,0,0])  
  union() {
      translate([0,-35,-1.7 ]) scale([0.5, 0.5, 1.1]) import ("defensestorm.stl", convexity = 4);
      translate([0,-60,3.5]) 
      rotate([90,0,0]) 
        cylinder(h=70, r=6);
  }
}