$fn=6;
r2d= 180/PI;

rads_per_step = 1.618/12;

firststep = 3;
stepsize = 1.25;
maxsteps = 1000;

minsize = 4;

alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "E", "O", "A", "I", "R", "S", "T", "H", "M", "D", "E", "E", "O", "A", "I", "R", "S", "T", "H", "M", "D", "E", "C", "C", "K", "K", "T", "R", "L", "L", "L", "N", "N", "N", "N",  "E", "O", "A", "I", "R", "S", "T", "H", "M", "D", "E", "E", "O", "A", "I", "R", "S", "T", "H", "M", "D", "E", "C", "C", "K", "K", "T", "R", "L", "L", "L", "N", "N", "N", "N"];

module basePrimitive(size) {
//   sphere(size);
    
    rotate([0,60,0])
    cube(size=[14,14,10]);
    scale(1) translate([15,4,-6]) rotate([60,0,90]) linear_extrude(2) text(alphabet[rands(1,len(alphabet),1)[0]], font="Times New Roman:style=Bold");
}

function dist_from_center(i) = 110-i/10 + ((i > 600) ? 1.5*(i-600)/12 : 0);
function height(i) = i/10*log(i);
function size(i) = max(i/log(i)/8, minsize);

for(i = [firststep:stepsize:maxsteps]) {
    rotate([0,0, rads_per_step*r2d*i%360])
    translate([dist_from_center(i), 0, height(i)])
    basePrimitive(size(i));
}

translate([0,0,-15]) cylinder(r=122, h=15, $fn=36);


//
////bowl
//
//for(i = [firststep:stepsize:maxsteps]) {
//    rotate([0,0, rads_per_step*r2d*i%360])
//    translate([dist_from_center(i), 0, height(i)])
//    basePrimitive(size(i));
//}
//
//for(i = [firststep:stepsize:maxsteps/8]) {
//    rotate([180,0,0])
//    translate([0,0,-30])
//    rotate([0,0, rads_per_step*r2d*i%360])
//    translate([dist_from_center(i), 0, height(i)])
//    basePrimitive(size(i));
//}



//
//for(i = [-99:stepsize:0]) {
//    rotate([0,0, rads_per_step*r2d*i%360])
//    translate([dist_from_center(i), 0, height(i)])
//    basePrimitive(size(i+100));
//}