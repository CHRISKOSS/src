grits = ["320", "400", "600", "1000", "1500", "2000", "2500", "3000", "3500"];

difference() {
    cube([95, 55, 25]);
    translate([1,5,5]) cube([93, 45, 27]);
    for(i = [0:1:8]) {
        translate([-2+10*i, 6.5, 50]) rotate([0, 80, 0]) pad();

    }
    translate([0, 55/2, 25]) rotate([0, 90,0]) cylinder(r=15, h=100);
    
    translate([8, 8, 0.4]) magnet();
    translate([8, 55-8, 0.4]) magnet();
    translate([95-8, 8, 0.4]) magnet();
    translate([95-8, 55-8, 0.4]) magnet();
}

//Labels
for(i = [0:1:8]) {
        translate([10+10*i, 0, 10]) rotate([0,-10,0]) rotate([0,-90, 90]) linear_extrude(1) scale(0.45) text(grits[i]);
        translate([7+10*i, 55, 23]) rotate([180,0,0]) rotate([0,10,0]) rotate([0,-90, 90]) linear_extrude(1) scale(0.45) text(grits[i]);
    }

module pad() {
    linear_extrude(5.5) hull() {
    circle(5);
    translate([42,0,0]) circle(5);
    translate([0,42,0]) circle(5);
    translate([42,42,0]) circle(5);
    }
}



module magnet() {
    cylinder(r=4.5, h=1.4);
}