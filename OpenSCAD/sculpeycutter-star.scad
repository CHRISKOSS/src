//printed one at 0.3 scale, too small for use
// second at 0.7

sculpeyCutter(0.7);
//translate([40,0,0]) sculpeyCutter(0.3);
//translate([-45,0,0]) sculpeyCutter(0.4);
font = "Cooper Black:style=Regular";
//font = "Californian FB:style=Italic";
offset = 0;

module shapePath(scal) {
//    scale(15) rotate([0,180,0]) offset(offset) text(word, spacing=0.8, font=font);
    scale(scal) import(file ="star.svg", center = true, dpi = 96);
}

module sculpeyCutter(
   scal, 
   height = 10,
   lip = 5,
   wallwidth = 0.8
  
) {
    
    difference() {
    union()
        {
            linear_extrude(wallwidth) offset(lip) shapePath(scal);
            
            linear_extrude(height) offset(wallwidth) shapePath(scal);
            
        }
    
    translate([0,0,-1]) linear_extrude(height+2) shapePath(scal);
    }
}

