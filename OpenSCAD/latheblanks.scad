$fn = 90;

in_to_mm = 25.4;
od = 5/8 * in_to_mm;
id = 7;
height = 2.5 * in_to_mm;

difference() {
cylinder(d=od, h=height);
cylinder(d=id, h=height);
}