include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <BOSL/shapes.scad>
use <BOSL/masks.scad>
use <BOSL/math.scad>


// ### Making shapes

dihedral = 116.565;
pi=3.14159;

module box(size) {
  cube([2*size, 2*size, size], center = true); 
}

module dodecahedron(size) {
  dihedral = 116.565;
  intersection(){
    box(size);
    intersection_for(i=[1:5])  { 
      rotate([dihedral, 0, 360 / 5 * i])  box(size); 
    }
  }
}

module icosahedron(size) {
  dihedral = 138.19;
  intersection(){
    box(size);
    intersection_for(i=[1:5])  { 
      rotate([dihedral, 0, 360 / 3 * i])  box(size);  
    }
  }
}

// ### Done making shapes
// ### Begin archive

module rhombus() {
  xrot(-a)
    yrot(30)
    back(thk)
    left(s)
    xrot(90)
    linear_extrude(thk)
    polygon([[0,0],[s/2,sin(60)*s],[s/2+s,sin(60)*s], [s,0]]);
}

module rhombus_neg() {
  xrot(-a)
    yrot(30)
    left(s)
    xrot(90)
    linear_extrude(thk)
    polygon([[0,0],[s/2,sin(60)*s],[s/2+s,sin(60)*s], [s,0]]);
}

module hexa() {
  difference() {

    difference() {
      union() {
        yrot(120)
          rhombus();

        yrot(-120)
          rhombus();

        rhombus();
      }
      yrot(120)
        rhombus_neg();

      yrot(-120)
        rhombus_neg();

      rhombus_neg();
    }

    /* back(s) */
    /* cuboid([s*3,s*3,s*3], align=V_BACK); */
  }
}

module hexa_neg() {

  union() {
    /* yrot(120) */
    /*   rhombus_neg(); */

    /* yrot(-120) */
    /*   rhombus_neg(); */

    rhombus_neg();
  }
}
module layer() {

  difference() {
    for (i=[0:count], deg = 360/count * i, x=circle_r*cos(deg), y=circle_r*sin(deg)) {
      right(x)
        back(y)
        zrot(deg+90)
        hexa();
    }
    for (i=[0:count], deg = 360/count * i, x=circle_r*cos(deg), y=circle_r*sin(deg)) {

      right(x)
        back(y)
        zrot(deg+90)
        hexa_neg();
    }
  }
}

/* d = 20; */
/* r = 20; */
/* s = 20; */
/* a = 10; */
/* circle_r = s*3.188; */
/* count = 32; */
/* thk = s*3; */
/* height = 10; */
/* //layer_up = s*.398; */
/* //layer_up = s*.2; */
/* layer_up = s*0.3979; */
/* //shift_down = s*0.04; */
/* shift_down = s*0.118; */


// ### End archive


s = 20 * 32/24; // size, given to dodecahedron()
circle_r = s*2.2593;
count = 24;
height = 4; // number of layers (sorda, there's an extra one b/c the ends get chopped off)
shift_down = s*-0.118; // How much you lower the entire thing before everything -z gets chopped
layer_up = s*0.3979; // How much to raise the next layer
bottom_slice = height*s*0.90; // where to chop the top

// dodecahedron layer
module dd_layer() {
  zring(n=count, r=circle_r)
    zrot(90)
    //xrot((180-dihedral))
    xrot(0)
    dodecahedron(s);
}

top_half(s=circle_r*10)
bottom_half(s=circle_r*100, z=bottom_slice) 
down(shift_down)
difference() {
  union() {
    for (h=[0:height]) {
      if (h % 2 == 1) {
        zrot(360/count/2)
          up(layer_up*h)
          dd_layer();
      } else {
        up(layer_up*h)
          dd_layer();
      }
      cyl(l=height*s*.4, r1=circle_r, r2=circle_r, align=V_UP);
    }
  }
}

