use <MCAD/boxes.scad>;
difference() {
    roundedBox([317/14, 154/14, 4.1], 1, false);
    translate([0,0,4]) roundedBox([22, 10, 4], 1); 
//    translate([0,0,-4]) roundedBox([22, 10, 4], 1); 
    translate([-7.5,0,-3]) cylinder(h=6, r=2);
}
translate([-5,-2.4,0]) scale(0.5) linear_extrude(4.5) text("Alan");
//rotate([180,0,0]) translate([-7,-2,0]) scale(0.5) linear_extrude(4.5) text("Alan");

$fn=24;

translate([0,0,-0.9]) difference() {
translate([317/14-177/28,0,0]) roundedBox([177/14, 87/14, 2.3], 0.5);
    
translate([317/14-177/28,0,0]) roundedBox([190/14, 59/14, 2.4], 0.5, true); 
    
    
   translate([20,0,3]) rotate([0,10,0]) roundedBox([10, 10, 4.1], 1, true);
   translate([20,0,-3]) rotate([0,-10,0]) roundedBox([10, 10, 4.1], 1, true);
    
 rotate([90,0,0]) translate([20,0,5]) rotate([0,10,0]) roundedBox([10, 10, 4.1], 1, true);
 rotate([-90,0,0]) translate([20,0,5]) rotate([0,10,0]) roundedBox([10, 10, 4.1], 1, true);
}