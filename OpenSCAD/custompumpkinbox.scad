
//$fn=32; 
$fn=16;
h=9.6;

//scale([1,1,0.7])


stem_rot = 15;
stem_stepheight = 1.5;

//switch top or base
//intersection() {
difference() {    
    
union() {
//stem
 translate([0,0,h-3]) {
color("DarkGreen") cylinder(r1=3,r2=1.5, h=2);
translate([0,0,stem_stepheight]) rotate([stem_rot,0,0]) {
color("DarkGreen") cylinder(r1=1.5,r2=1, h=2);
translate([0,0,stem_stepheight]) rotate([stem_rot,0,0]) {
color("DarkGreen") cylinder(r1=1,r2=0.8, h=2);
translate([0,0,stem_stepheight]) rotate([stem_rot,0,0]) {
color("DarkGreen") cylinder(r1=0.8,r2=0.6, h=2);
}
}
}
}

translate([0,0,h-5]) cylinder(r1=3,r2=3, h=2);

////    rotate([5,0,-90]) intersection() {
////        
//    rotate([0,180,0]) rotate([-90,0,0]) linear_extrude(15) scale(.12) import(file = "ds.svg", center = true, dpi = 96);
////    translate([0,-1,-h/2])
////    scale(0.99)color("DarkOrange") Pumpkin();
////    }
// Pumpkin body
//color("DarkOrange") Pumpkin();
scale([1,1,1])
difference() {
    union() {
        difference() {
            scale(1.05) color("DarkOrange") Pumpkin();
            scale(1.0) color("DarkOrange") Pumpkin();
        }
        
//        scale(1)  color("DarkOrange") Pumpkin();
        difference() {
            scale(1.2) color("DarkOrange") Pumpkin();
            scale(1.0) color("DarkOrange") Pumpkin();
            translate([0,0,6]) 
            rotate([0,180,0]) rotate([-90,0,90]) linear_extrude(15) translate([-7.5, -10, 0]) resize([15,10,0]) text("GRAMMY", font="Arial Rounded MT Bold:style=Regular", spacing=1.1);
           
//            rotate([0,180,0]) rotate([-90,0,0]) linear_extrude(15) scale(.12) import(file = "ds.svg", center = true, dpi = 96);
//            cube([h*2,h*2,h*2]);
//            scale(1.02) color("DarkOrange") Pumpkin();



            }
            
        }
        }
//scale(0.95) Pumpkin();
//cube([h*2,h*2,h*2]);
    
    
    }
    
    #scale([0.95,1.05,1]) cylinder(d1=15, d2=25, h=13);
}


slices = 10;
slice_rotation = 360/slices;
rough_radius = 10;
rib_width = 2;
rib_r = rib_width/2;


module Pumpkin(){
    translate([0,0,-h/2]) {
    for (a =[0:slices]){

    slice(slice_rotation*a);
    }
}

};

module slice(rotation){
    rotate([0,0,rotation])
    translate([rough_radius, 0, 0]) 
    subslice();
};

module subslice() {
    hull() {
        translate([-1.5,rib_r,h/2])
        scale([.7,0.3,1.5])
        
        sphere(d=h);
        translate([-1.5,-rib_r,h/2])
        scale([.7,0.3,1.5])
        sphere(d=h);
        
        translate([-1,0,h/2])
        rotate([0,0,90]) 
        scale([1,0.3,1.5])
        sphere(d=h);
        
        translate([-5,0,h+1])
        rotate([90,0,0]) 
        scale([.7,0.3,1])
        sphere(d=h);
        
        translate([-h,0,0-1])
        rotate([90,0,0]) 
        scale([.7,0.3,1])
        sphere(d=h);
    }
};


//    rotate([(rands(0,10,1)[0]-5)/5, (rands(0,10,1)[0]-5)/5,0]) 
//    scale([1,1,0.8])  