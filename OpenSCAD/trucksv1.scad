

difference() {
union() {
import("trucksbase.stl", convexity=3);

scale([1,1,0.9]) hull() {
translate([40, 0, 29]) sphere(30);
translate([-40, 0, 29]) sphere(30);
}
}

translate([0,0,-100]) cube(200, center=true);
}

