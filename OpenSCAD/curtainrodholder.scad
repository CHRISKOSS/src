//Curtain rod holder to hang all my filament in my office


rod_diameter = 21;
rod_holder_wall_thickness = 3;
dist_from_wall_front = 150;
dist_from_wall_back = 50;
hangar_height = 80;
hangar_wall_thickness = 5;
width = 20;
difference(){
union() {
//frame
hull() {
cube([hangar_height, hangar_wall_thickness, width]); 
cube([rod_diameter/2+rod_holder_wall_thickness, dist_from_wall_front, width]);
}
//rod holder outers
translate([0,dist_from_wall_front,0]) rod_holder("outer");
translate([0,dist_from_wall_back,0]) rod_holder("outer");
}
//negatives

//rod holder inners
translate([0,dist_from_wall_front,0]) rod_holder("inner");
translate([0,dist_from_wall_back,0]) rod_holder("inner");

 translate([hangar_height*0.75, -10, width/2]) rotate([-90,0,0]) screw_hole();
 translate([hangar_height*0.2, -10, width/2]) rotate([0,0,-30]) rotate([-90,0,0]) screw_hole();
//rotate([-90,0,0]) screw_hole();
}






screw_shaft_dia = 5.5;
screw_head_dia = 11;
screw_shaft_len = 40;

//!screw_hole();

module screw_hole() {
    cylinder(r=screw_shaft_dia/2, h= screw_shaft_len);
    translate([0,0,screw_shaft_len]) cylinder(r1=screw_shaft_dia/2, r2=screw_head_dia/2, h= 5);
    translate([0,0,screw_shaft_len+5]) 
    cylinder(r=screw_head_dia/2, h= screw_shaft_len);
}

module rod_holder(type) {
    if(type == "outer"){
    cylinder(r=rod_diameter/2+rod_holder_wall_thickness, h=width);
    } else if(type == "inner") { 
    cylinder(r=rod_diameter/2, h=width); 
    }
}