$fn = 6;
projection() rotate([0,0,180]) rotate([180,0,0]) color([0,0,0]) difference() {
union() { 
    linear_extrude(7) import(file = "ds.svg", center = true, dpi = 96);

rotate([0,0,30]) cylinder(r=55, h = 5);}

difference() {
rotate([0,0,30]) cylinder(r=53, h = 2);
rotate([0,0,30]) cylinder(r=51, h = 2);
}
translate([42, 15, 1.6]) rotate([0, 180, 0]) scale([1, 1, 1])  union() {
linear_extrude(1.8) translate([3, 0, 0]) text("COMMUNITY", font="Liberation Sans:style=Bold", size=9, $fn=100);
linear_extrude(1.8) translate([32, -15, 0]) text("OF", font="Liberation Sans:style=Bold", size=9, $fn=100);
linear_extrude(1.8) translate([21, -27, 0]) text("TRUST", font="Liberation Sans:style=Bold", size=9, $fn=100);
linear_extrude(1.8) translate([3, -42, 0]) text("DefenseStorm", size=8.5, font="Liberation Sans:style=Bold", $fn=100);
linear_extrude(1.8) translate([29, -54, 0]) text("2021", size=8.5, font="Liberation Sans:style=Bold", $fn=100);
}

translate([-60,-60,0.1]) cube([120,120,100]);
}