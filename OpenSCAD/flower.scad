$fn = 36;
layerheight = 5;
radii = [30, 25, 20, 16, 14, 18, 22, 26.5, 29, 31.2, 33, 35, 36, 36.5, 37, 37.2, 37, 36.5, 36, 35, 33, 29, 26.5, 22, 18, 15,14, 12, 11, 12];

bottom_thickness = 6.5;
wall_thickness = 12;
bottom_layer = 4;


//
//for (i=[0:1:len(radii)-2]) {
//    difference() {
//    // outer wall
//    translate([0,0, layerheight * i])
//    cylinder(h=layerheight, r1=radii[i], r2=radii[i+1]);
//    translate([0,0, layerheight * i])
//    // inner wall
//    cylinder(h=layerheight, r1=radii[i]-wall_thickness, r2=radii[i+1]-wall_thickness);
//    }
//}

scale([1,1,1.2])
for (i=[25:1:70]) {
   rotate([0, i/2, 126*i]) petal(i);
}

module petal(i) {
    rotate([0,180,0])
    translate([10,-25,0])
    scale([1-0.005*i, 1, 1])
    intersection() {
        scale([5, 1, 1]) linear_extrude(15) square(50);
        difference() {
          translate([0, 25, 0]) rotate([0, 99, 0])
            cylinder(h=120, r=15);
           translate([0, 25, 0]) rotate([0, 99, 0])
            cylinder(h=120, r=14);
        }
    }
}
intersection() {
sphere(30);
translate([0,0,15]) cube(60, center=true);
}
